import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  cntrlValue = ['val1', 'val2'];
  ratingValue: Number = 3;
  config = {
    valueField: 'value',
    options: [{ value: 'val1' }, { value: 'val2' }, { value: 'val3' }, { value: 'val4' }],
    mode: 'multiSelect'
  };

  fun(ev) {

  }
  checkValue(drop) {
    // debugger;
  }
  ngOnInit() {
    this.ratingValue = 4;
    this.cntrlValue = ['val1', 'val2'];

  }
}
