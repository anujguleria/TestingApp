import {Directive, ContentChild} from "@angular/core";

@Directive({
    selector:'ag-option'
})
export class AgOption{
    @ContentChild('agOption') agOptionTemplate;

}