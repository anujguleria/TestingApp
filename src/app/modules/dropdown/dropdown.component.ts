import { Component, Input, Inject, ViewChildren, QueryList, forwardRef, Directive } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormControl, NG_VALIDATORS } from '@angular/forms';
import * as _ from 'underscore';

function customRequiredValidator(control: FormControl) {
    if (typeof (control.value) === 'object') {
        if (control.value && control.value.length === 0) {
            return {
                customRequired: {
                    value: control.value.length
                }
            };
        }
    } else {
        if (control.value !== undefined && control.value.length === 0) {
            return {
                customRequired: {
                    value: control.value.length
                }
            };
        }
    }
    return null;

}
@Directive({
    selector: '[customRequired][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useValue: customRequiredValidator,
            multi: true
        }
    ]
})
export class CustomRequiredValidator {

}

@Component({
    selector: 'ag-dropdown',
    templateUrl: './dropdown.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DropDownComponent),
            multi: true
        }
    ],
    styleUrls: ['./dropdown.component.scss']
})
export class DropDownComponent implements ControlValueAccessor {
    dropDownOpen = false;
    @Input() valueField: string;
    @Input() disabled: boolean;

    private _options: any[];
    @Input() get options() {
        return this._options;
    }
    set options(value) {
        this._options = value;
    }
    private _mode;
    @Input() get mode() {
        return this._mode;
    }
    set mode(value) {
        this._mode = value;
    }
    get selectedCount() {
        return '';
    }
    constructor() {


    }
    // Function to call when the rating changes.
    onChange = (selectedValues: string[]) => {

    }

    // Function to call when the input is touched (when a star is clicked).
    onTouched = () => {

    }
    get value(): any {
        if (this.mode === 'multiSelect') {
            return _.pluck(_.where(this.options, { 'isSelected': true }), this.valueField);
        } else {
            const vals = _.pluck(_.where(this.options, { 'isSelected': true }), this.valueField);
            if (vals.length > 0) {
                return vals[0];
            } else {
                return '';
            }
        }
    }
    setValue(options) {
        const keyFields = _.pluck(this.options, this.valueField);
        if (options) {
            if (this.mode === 'multiSelect') {
                options.forEach(opt => {
                    const indexOfOption = keyFields.indexOf(opt);
                    if (indexOfOption !== -1) {
                        this.options[indexOfOption]['isSelected'] = true;
                    }
                });
            } else {
                const indexOfOption = keyFields.indexOf(options);
                if (indexOfOption !== -1) {
                    this.options[indexOfOption]['isSelected'] = true;
                }
            }
        }
    }
    changeModel(option) {
        const tempVal = option['isSelected'];
        if (this.mode !== 'multiSelect') {
            this.options.forEach(function (opt) {
                opt['isSelected'] = false;
            });
        }
        option['isSelected'] = !tempVal;
        this.writeValue(this.value);
    }
    writeValue(value): void {
        if (value !== undefined) {
            this.setValue(value);
            this.onChange(value);
        }

    }
    // Allows Angular to register a function to call when the model (rating) changes.
    // Save the function as a property to call later here.
    registerOnChange(fn: (selectedValues: string[]) => void): void {
        this.onChange = fn;
    }

    // Allows Angular to register a function to call when the input has been touched.
    // Save the function as a property to call later here.
    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    // Allows Angular to disable the input.
    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }
}
