import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropDownComponent, CustomRequiredValidator } from './dropdown.component';
import { FormsModule } from '@angular/forms';
import { SharedPipesModule } from './../pipes/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedPipesModule

  ],
  declarations: [
    DropDownComponent,
    CustomRequiredValidator
  ],
  exports: [DropDownComponent, CustomRequiredValidator]
})
export class DropdownModule { }
