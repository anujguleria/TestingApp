import { Observable } from 'rxjs/Observable';

export abstract class IOptionsProvider {
    abstract getOptions(entityName: string): Observable<any>;
}
